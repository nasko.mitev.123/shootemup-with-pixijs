import { Game } from './game/Game';

// Create a new instance of the game
const game = new Game();

// Initialize the game
game.init();

// Start the game loop
game.start();