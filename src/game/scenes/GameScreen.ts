import { Container, Sprite, Texture, Text} from "pixi.js";
import { Game } from '../Game';
import { Hero } from '../entities/Hero';
import { Obstacle } from '../entities/Obstacle';
import { Opponent } from '../entities/Opponent';
import { InputManager } from '../utils/InputManager';
import { CollisionManager } from '../utils/CollisionManager';
import { ScoreManager } from '../utils/ScoreManager';
import { Background } from '../background/Background';
import { Spawner } from '../utils/Spawner';
import { EntityType } from '../utils/Enums';
import { GroundOpponent } from '../entities/GroundOpponent';

export class GameScreen {
  public game: Game;
  public container: Container;
  private hero: Hero;
  private obstacles: Obstacle[];
  private opponents: Opponent[];
  private groundOpponents: GroundOpponent[];
  private inputManager: InputManager;
  private collisionManager: CollisionManager;
  private scoreManager: ScoreManager;
 
  private scoreText: Text;
  private background: Background;
  private playerLives: Text;
  private playerBombs: Text;
  private opponentSpawner: Spawner;
  private groundOpponentSpawner: Spawner;
  private obstacleSpawner: Spawner;
 
  constructor(container: Container, game: Game) {
    this.container = container;
    this.game = game;
    this.inputManager = new InputManager();
   
    this.collisionManager = new CollisionManager();
    this.scoreManager = new ScoreManager();
    this.background = new Background(Texture.from('../../assets/sprites/background.png'), 0.3);
    
   
    const playerSprite = Sprite.from('../../assets/sprites/hero.png');
    this.hero = new Hero(200, 200, playerSprite, this.inputManager);
    this.obstacles = [];
    this.opponents = [];
    this.groundOpponents = [];
    

    this.scoreText =  new Text(`Score: ${this.scoreManager.getScore().toString()}`, { fill: 0xffffff });
    this.playerLives =  new Text(`Lives: ${this.hero.lives.toString()}`, { fill: 0xffffff });
    this.playerBombs =  new Text(`Bombs: ${this.hero.bombsAvailable.toString()}`, { fill: 0xffffff });
    this.opponentSpawner = new Spawner();
    this.obstacleSpawner = new Spawner();
    this.groundOpponentSpawner = new Spawner();
    this.setupGame();
  }

  private setupGame(): void {
    // Set up the initial game state, including creating obstacles and opponents
   
    this.background.addToContainer(this.container);
    this.inputManager.initInputHandling();

    this.scoreManager.resetScore();
    this.scoreText.position.set(10, 10); 
    this.playerLives.position.set(10,40);
    this.playerBombs.position.set(10,70);
    this.container.addChild(this.scoreText);
    this.container.addChild(this.playerLives);
    this.container.addChild(this.playerBombs);
    
  }

  public update(delta: number): void {
    // Update the game logic
    this.updateEntities(delta);
    this.spawnEntities(delta);

    this.checkBulletEnemyCollisions();
    this.checkPlayerCollisions();
    this.background.update();
  }

  private updateEntities(delta: number): void {
    // Update all game entities, including the hero, obstacles, and opponents
    this.hero.update();
    this.obstacles.forEach((obstacle) => obstacle.update(delta));
    this.opponents.forEach((opponent) => opponent.update(delta));
    this.groundOpponents.forEach((groundOpponents) => groundOpponents.update(delta));
    
  }
  private spawnEntities(delta:number): void
  {
    this.opponentSpawner.spawn(delta,this.opponents,this.container,EntityType.Opponent,3);
    this.groundOpponentSpawner.spawn(delta,this.groundOpponents,this.container,EntityType.GroundOpponent,(Math.random()*30));
    this.obstacleSpawner.spawn(delta,this.obstacles,this.container,EntityType.Obstacle,8);
  }

  public checkBulletEnemyCollisions(): void {
    for (let i = this.opponents.length - 1; i >= 0; i--) {
      const opponent = this.opponents[i];
  
      for (let j = this.hero.bullets.length - 1; j >= 0; j--) {
        const bullet = this.hero.bullets[j];
        if (this.collisionManager.checkCollision(bullet, opponent)) {
        bullet.destroy();
        opponent.destroy();
        opponent.bullets.forEach((bullet) => bullet.destroy());
        this.hero.bullets.splice(j, 1);
        this.opponents.splice(i, 1);
        this.scoreManager.increaseScore(200);
        break;
        }
      }

      for (let j = this.hero.bombs.length - 1; j >= 0; j--) {
        const bomb = this.hero.bombs[j];
        if (this.collisionManager.checkCollision(bomb, opponent)) {
        bomb.destroy();
        opponent.destroy();
        this.hero.bombs.splice(j, 1);
        this.opponents.splice(i, 1);
        this.scoreManager.increaseScore(200);
        break;
        }
      }
    }
  }

  public checkPlayerCollisions(): void {
      const hero = this.hero;
      for (let i = this.opponents.length - 1; i >= 0; i--) {
        const opponent = this.opponents[i];
  
        if (this.collisionManager.checkCollision(hero, opponent)) {
        this.hero.lives --;
        opponent.destroy();
        opponent.bullets.forEach((bullet) => bullet.destroy());
        this.opponents.splice(i, 1);
        if(this.hero.lives<=0)
        {
          this.EndGame();
        }
        break;
        }
      }
      for (let i = this.opponents.length - 1; i >= 0; i--) {
        const opponent = this.opponents[i];
        for (let j = opponent.bullets.length - 1; j >= 0; j--) {
          const bullet = opponent.bullets[j];
          if (this.collisionManager.checkCollision(hero, bullet)) {
          this.hero.lives --;
          bullet.destroy();
          opponent.bullets.splice(j, 1);
          if(this.hero.lives<=0)
          {
            this.EndGame();
          }
          break;
          }
      }
    }

      for (let i = this.groundOpponents.length - 1; i >= 0; i--) {
        const groundOpponent = this.groundOpponents[i];
        if (this.collisionManager.checkCollision(hero, groundOpponent)) {
        this.hero.lives --;
        groundOpponent.destroy();
        this.groundOpponents.splice(i, 1);
        if(this.hero.lives<=0)
        {
          this.EndGame();
        }
        break;
        }

        for (let j = this.hero.bombs.length - 1; j >= 0; j--) {
          const bomb = this.hero.bombs[j];
          if (this.collisionManager.checkCollision(bomb, groundOpponent)) {
          bomb.destroy();
          groundOpponent.destroy();
          this.hero.bombs.splice(j, 1);
          this.groundOpponents.splice(i, 1);
          this.scoreManager.increaseScore(200);
          break;
          }
        }
      }

      for (let i = this.obstacles.length - 1; i >= 0; i--) {
        const obstacle = this.obstacles[i];
        if (this.collisionManager.checkCollision(hero, obstacle)) {
        this.hero.lives --;
        obstacle.destroy();
        this.obstacles.splice(i, 1);
        if(this.hero.lives<=0)
        {
          this.EndGame();
        }
        break;
        }

        for (let j = this.hero.bullets.length - 1; j >= 0; j--) {
          const bullet = this.hero.bullets[j];
          if (this.collisionManager.checkCollision(bullet, obstacle)) {
          bullet.destroy();
          obstacle.destroy();
          this.hero.bullets.splice(j, 1);
          this.obstacles.splice(i, 1);
          break;
          }
        }

        for (let j = this.hero.bombs.length - 1; j >= 0; j--) {
          const bomb = this.hero.bombs[j];
          if (this.collisionManager.checkCollision(bomb, obstacle)) {
          bomb.destroy();
          obstacle.destroy();
          this.hero.bombs.splice(j, 1);
          this.obstacles.splice(i, 1);
          break;
          }
        }
      }
    }
  
  public render(): void {
    // Render the game entities and other visual elements
    this.hero.render(this.game.app);
    this.obstacles.forEach((obstacle) => obstacle.render(this.game.app));
    this.opponents.forEach((opponent) => opponent.render(this.game.app));
    this.groundOpponents.forEach((groundOpponent) => groundOpponent.render(this.game.app));
    this.hero.bullets.forEach((bullet) => bullet.render(this.game.app));
    this.hero.bombs.forEach((bomb) => bomb.render(this.game.app));
    this.opponents.forEach((opponent) => opponent.bullets.forEach((bullet) => bullet.render(this.game.app)));
      // Update UI
    this.scoreText.text = `Score: ${this.scoreManager.getScore().toString()}`;
    this.playerLives.text =  `Lives: ${this.hero.lives.toString()}`;
    this.playerBombs.text =  `Bombs: ${this.hero.bombsAvailable.toString()}`;
  }

  private destroyAll(): void {
    // Destroy the hero
    this.hero.destroy();
    // Destroy the obstacles
    this.obstacles.forEach((obstacle) => obstacle.destroy());
    this.obstacles = [];
    this.opponents.forEach((opponent) => opponent.bullets.forEach((bullet) => bullet.destroy()));
    // Destroy the opponents
    this.opponents.forEach((opponent) => opponent.destroy());
    this.opponents = [];
    // Destroy the ground opponents
    this.groundOpponents.forEach((groundOpponent) => groundOpponent.destroy());
    this.groundOpponents = [];
    // Destroy the bullets
    this.hero.bullets.forEach((bullet) => bullet.destroy());
    this.hero.bullets = [];
    // Destroy the bombs
    this.hero.bombs.forEach((bomb) => bomb.destroy());
    this.hero.bombs = [];
    
    this.container.removeChild(this.playerLives);
    this.container.removeChild(this.playerBombs);
  }

  public EndGame(): void{
    this.destroyAll();
    this.game.showEndScreen();
  }
}
