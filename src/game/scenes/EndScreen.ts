import { Container, Sprite, Texture, Text} from "pixi.js";
import { Game } from '../Game';
import { GameConfig } from '../config/GameConfig';

export class EndScreen {
  private game: Game;
  public score: number;
  private container: Container;
  private restartButton: Sprite;

  constructor(container: Container, game: Game, score: number) {
    this.container = container;
    this.game = game;
    this.score = score;
    this.restartButton = this.createRestartButton();
    this.setupScreen();
  }

  private createRestartButton(): Sprite {
    // Create and configure the restart button sprite using PIXI.js
    const restartButtonTexture = Texture.from('../../assets/sprites/restartButton.png');
    const restartButton = new Sprite(restartButtonTexture);
    restartButton.interactive = true;
    restartButton.buttonMode = true;
    restartButton.on('pointerdown', () =>{
      this.game.restartGame();
      this.destroy();
    });

    return restartButton;
  }

  private setupScreen(): void {
     // Center the play button horizontally
     const buttonOffsetX = (GameConfig.screenWidth - this.restartButton.width) / 2;
     this.restartButton.x = buttonOffsetX;
 
     // Center the play button vertically
     const buttonOffsetY = (GameConfig.screenHeight - this.restartButton.height) / 2;
     this.restartButton.y = buttonOffsetY;
     //console.log("container width: " + this.container.width + " Container height: " + this.game.app.stage.height);
 
      // Set the anchor point of the play button to (0.5, 0.5)
     this.restartButton.anchor.set(0.5);
 
     // Add the playButton to the screen or container
     this.container.addChild(this.restartButton);
  }



  public update(): void {
    // Update any dynamic elements on the end screen
  }

  public render(): void {
    // Clear the container
  this.container.removeChildren();

  // Render the score
  const scoreText = new Text(`Score: ${this.score}`);
  scoreText.anchor.set(0.5);
  scoreText.position.set(this.container.width / 2, this.container.height / 2 - 50);
  this.container.addChild(scoreText);

  // Render the restart button
  this.restartButton.position.set(GameConfig.screenWidth / 2, GameConfig.screenHeight / 2);
  this.container.addChild(this.restartButton);
  }

  public destroy(): void {
    // Remove the restart button from the screen or container
    this.container.removeChild(this.restartButton);
  }
}
