import { Container, Sprite, Texture} from "pixi.js";
import { Game } from '../Game';
import { GameConfig } from '../config/GameConfig';

export class StartScreen {
  private game: Game;
  private container: Container;
  private playButton: Sprite;

  constructor(container: Container, game: Game) {
    this.container = container;
    this.game = game;
    this.playButton = this.createPlayButton();
    // Additional setup specific to the start screen
    this.setupScreen();
  }

  private createPlayButton(): Sprite {
    const playButtonTexture = Texture.from('../../assets/sprites/playButton.png');
    const playButton = new Sprite(playButtonTexture);
    playButton.interactive = true;
    playButton.buttonMode = true;
    playButton.on('pointerdown', () => {
      this.destroy();
      this.game.showGameScreen();
    });

    return playButton;
  }

  private setupScreen(): void {
    // Center the play button horizontally
    const buttonOffsetX = (GameConfig.screenWidth - this.playButton.width) / 2;
    this.playButton.x = buttonOffsetX;

    // Center the play button vertically
    const buttonOffsetY = (GameConfig.screenHeight - this.playButton.height) / 2;
    this.playButton.y = buttonOffsetY;
    //console.log("container width: " + this.container.width + " Container height: " + this.game.app.stage.height);

     // Set the anchor point of the play button to (0.5, 0.5)
    this.playButton.anchor.set(0.5);

    // Add the playButton to the screen or container
    this.container.addChild(this.playButton);
  }

  public update(): void {
    // Update any dynamic elements on the start screen
  }

  public render(): void {
    // Clear the container to remove any previous rendering
    this.container.removeChildren();

    // Render the start button
    this.container.addChild(this.playButton);
  }

  public destroy(): void {
    // Remove the playButton from the screen or container
    this.container.removeChild(this.playButton);
  }
}
