import { Ticker, Container, Application } from "pixi.js";
import { StartScreen } from './scenes/StartScreen';
import { GameScreen } from './scenes/GameScreen';
import { EndScreen } from './scenes/EndScreen';
import { AssetLoader } from './utils/AssetLoader';
import { InputManager } from './utils/InputManager';
import { ScoreManager } from './utils/ScoreManager';
import { GameConfig } from './config/GameConfig';

export class Game {
  public app: Application;
  private currentScene: StartScreen | GameScreen | EndScreen;
  public assetLoader: AssetLoader;
  private inputManager: InputManager;
  private scoreManager: ScoreManager;

  constructor() {
    const screenWidth = GameConfig.screenWidth;
    const screenHeight = GameConfig.screenHeight;
    const paddingPercentage = 0.01; // 1% padding

    const appWidth = screenWidth * (1 - 2 * paddingPercentage);
    const appHeight = screenHeight * (1 - 2 * paddingPercentage);

    const appX = (screenWidth - appWidth) / 2;
    const appY = (screenHeight - appHeight) / 2;

    const canvas = document.getElementById('game-canvas') as HTMLCanvasElement;

    this.app = new Application({
      width: appWidth,
      height: appHeight,
      backgroundColor: 0x990000,
      view: canvas,
    });

    this.app.view.style.position = 'absolute';
    this.app.view.style.left = appX + 'px';
    this.app.view.style.top = appY + 'px';

    document.body.appendChild(this.app.view);

    this.assetLoader = new AssetLoader();
    this.inputManager = new InputManager();
    this.scoreManager = new ScoreManager();
    this.currentScene = new StartScreen(new Container(), this);
  }
  


  public async init(): Promise<void> {
    //await this.assetLoader.loadAssets(['../assets/']);
    this.inputManager.initInputHandling();
  }

  public start(): void {
    //this.showGameScreen();
    this.showStartScreen();
    //this.lastFrameTime = performance.now();
    this.gameLoop();
  }

  private gameLoop(): void {
    //const currentFrameTime = performance.now();
    const delta = Ticker.shared.elapsedMS;

    requestAnimationFrame(() => {
      this.update(delta);
      this.render();
      this.gameLoop();
    });
  }

  private update(delta: number): void {
    if (this.currentScene) {
      this.currentScene.update(delta);
    }

  }

  private render(): void {
    if (this.currentScene) {
      this.currentScene.render();
    }
  }

  public showStartScreen(): void {
    const startScreenContainer = new Container();
    this.currentScene = new StartScreen(startScreenContainer, this);
    // Additional setup specific to the start screen
    this.app.stage.addChild(startScreenContainer);
  }

  public showGameScreen(): void {
    const gameScreenContainer = new Container();
    this.currentScene = new GameScreen(gameScreenContainer, this);
    // Additional setup specific to the game screen
    this.app.stage.addChild(gameScreenContainer);
  }

  public showEndScreen(): void {
    const endScreenContainer = new Container();
    this.currentScene = new EndScreen(endScreenContainer, this, this.scoreManager.getScore());
    // Additional setup specific to the end screen
    this.app.stage.addChild(endScreenContainer);
  }

  public restartGame(): void {
    this.scoreManager.resetScore();
    this.showGameScreen();
  }
}
