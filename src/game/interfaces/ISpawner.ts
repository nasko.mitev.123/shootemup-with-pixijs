import { Container } from "pixi.js";

import { Entity } from '../entities/Entity';
import { EntityType } from '../utils/Enums';

export interface ISpawner {
  spawn(delta: number, entities: Entity[], container: Container, entityType: EntityType, spawnSpeed:number): void;
}
