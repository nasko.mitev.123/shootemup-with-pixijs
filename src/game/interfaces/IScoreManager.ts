export interface IScoreManager {
    getScore(): number;
    increaseScore(points: number): void;
    resetScore(): void;
  }
  