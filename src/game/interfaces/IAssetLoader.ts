import { Texture } from "pixi.js";

export interface IAssetLoader {
    loadAssets(assetPaths: string[]): Promise<void>;
    getTexture(textureName: string): Texture;
  }
  