export interface IInputManager {
    isKeyPressed(keyCode: string): boolean;
    isPointerPressed(): boolean;
  }
  