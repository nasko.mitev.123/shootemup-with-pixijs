import { Entity } from "../entities/Entity";

export interface ICollisionManager {
  checkCollision(entity1: Entity, entity2: Entity): boolean;
}
