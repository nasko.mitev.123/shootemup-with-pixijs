import { TilingSprite, Texture, Ticker, Container } from "pixi.js";

export class Background {
  private sprite: TilingSprite;
  private speed: number;
  private delta: number;

  constructor(texture: Texture, speed: number) {
    this.sprite = new TilingSprite(texture, window.innerWidth, window.innerHeight);
    console.log("inner width:" + window,innerWidth + " Inner height: " + window.innerHeight);
    this.speed = speed;
    this.delta = Ticker.shared.elapsedMS;
  }

  public addToContainer(container: Container): void {
    container.addChild(this.sprite);
  }

  public update(): void {
    this.move(this.delta);
  }

  private move(delta:number): void {
    const moveAmount = this.speed * delta;
    this.sprite.tilePosition.x -= moveAmount;
  }
}
