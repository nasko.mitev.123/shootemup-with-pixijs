import { Sprite } from "pixi.js";
import { Opponent } from './Opponent';

export class GroundOpponent extends Opponent {
  constructor(x: number, y: number, sprite: Sprite) {
    super(x, y, sprite);
    
    this._speed = 30; // Adjust the speed for the second opponent
  }

  public update(delta:number): void {
    super.update(delta);
  }

  public fireProjectile(): void {
    
  }

  public moveLeft(delta: number): void {
    
    super.moveLeft(delta);
  }
}