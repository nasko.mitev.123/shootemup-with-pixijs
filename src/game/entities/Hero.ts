import { Sprite } from "pixi.js";

import { Entity } from './Entity';
import { InputManager } from '../utils/InputManager';
import { Bullet } from './Bullet';
import { Bomb } from './Bomb';

export class Hero extends Entity {
  //Create an instance of the input manager
  private inputManager: InputManager;
  public bullets: Bullet[];
  public bombs: Bomb[];
  public shootSpeed: number = 20;
  public bombDropSpeed: number = 10;
  private lastBulletSpawnTime: number;
  private lastBombSpawnTimer: number;
  private bulletSpawnCooldown: number = 500; // Set the desired cooldown between bullet spawns (in milliseconds)
  public lives: number;
  public bombsAvailable: number;

  constructor(x: number, y: number, sprite: Sprite, inputManager: InputManager) {
    super(x, y, sprite);
    this.sprite = sprite;
    this.inputManager = inputManager;
    this.bullets = [];
    this.bombs = [];
    this.lastBulletSpawnTime = 0;
    this.lastBombSpawnTimer = 0;
    this.lives = 3;
    this.bombsAvailable = 5;
  }

  public update(): void {
    // Update hero logic based on input
    this.handleInput();
    this.updateBullets();
    this.updateBombs();
  }

  private handleInput(): void {
    //Create a variable that sets the movement speed of the hero
    let speed: number = 5; 

    if (this.inputManager.isKeyPressed('ArrowUp')) {
      this.y -= speed;
    } else if (this.inputManager.isKeyPressed('ArrowDown')) {
      this.y += speed;
    }
  
    if (this.inputManager.isKeyPressed('ArrowLeft')) {
      this.x -= speed;
    } else if (this.inputManager.isKeyPressed('ArrowRight')) {
      this.x += speed;
    }
  
    if (this.inputManager.isKeyPressed('Space')) {
      this.shoot();
    }
  
    if (this.inputManager.isKeyPressed('KeyZ')) {
      this.dropBomb();
    }
  }

  private shoot(): void {
    const currentTime = Date.now();
    if (currentTime - this.lastBulletSpawnTime >= this.bulletSpawnCooldown) {
    
    const bulletSprite = Sprite.from('../../assets/sprites/bullet01.png');
    const bullet = new Bullet(this.x, this.y, bulletSprite, this.shootSpeed);
    this.bullets.push(bullet);
  

    this.lastBulletSpawnTime = currentTime; // Update the last bullet spawn time
    }
  }

  private dropBomb(): void {
    const currentTime = Date.now();
    if (currentTime - this.lastBombSpawnTimer >= this.bulletSpawnCooldown && this.bombsAvailable>0) {
    this.bombsAvailable--;
    const bombSprite = Sprite.from('../../assets/sprites/bomb.png');
    const bomb = new Bomb(this.x, this.y, bombSprite, this.bombDropSpeed);
    this.bombs.push(bomb);
  

    this.lastBombSpawnTimer = currentTime; // Update the last bullet spawn time
    }
  }


  private updateBullets(): void {
    for (let i = this.bullets.length - 1; i >= 0; i--) {
      const bullet = this.bullets[i];
      bullet.update();
      // Remove bullets that are out of bounds or have collided with something
      if (bullet.isOutOfBounds()) {
        bullet.destroy();
        this.bullets.splice(i, 1);
      }
    }
  }
  private updateBombs(): void {
    for (let i = this.bombs.length - 1; i >= 0; i--) {
      const bomb = this.bombs[i];
      bomb.update();
      // Additional logic for bullet movement, collision detection, etc.
      // Remove bullets that are out of bounds or have collided with something
      if (bomb.isOutOfBounds()) {
        bomb.destroy();
        this.bombs.splice(i, 1);
      }
    }
  }

}
