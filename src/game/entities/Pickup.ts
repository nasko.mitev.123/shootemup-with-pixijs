import { Sprite } from "pixi.js";
import { Entity } from './Entity';

export class Pickup extends Entity {
  //TODO Add any additional properties specific to the Opponent
  protected _speed: number = 2; // Adjust the speed as needed
  constructor(x: number, y: number, sprite: Sprite) {
    super(x, y, sprite);
  }

  public get speed(): number {
    return this._speed;
  }

  public set speed(value: number) {
    this._speed = value;
  }

  public update(delta: number): void {
    //TODO Update opponent logic
    this.moveLeft(delta);
  }

  public moveLeft(delta: number): void {
    this.x -= this.speed/100 * delta;
    // Additional logic for handling leftward movement, such as boundary checks
  }
}
