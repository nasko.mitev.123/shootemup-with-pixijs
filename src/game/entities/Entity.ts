import { Sprite, Application, Rectangle } from "pixi.js";

export class Entity{
    protected x: number;
    protected y: number;
    protected sprite: Sprite;
  
    constructor(x: number, y: number, sprite: Sprite) {
      
      this.x = x;
      this.y = y;
      this.sprite = sprite;
    }
  
    public update(delta?: number): void{
      if(delta){
      }
    }
  
    public render(app: Application): void {
    // Add the sprite to the PIXI application's stage
     app.stage.addChild(this.sprite);

    // Set the position of the sprite based on the entity's coordinates
    this.sprite.position.set(this.x, this.y);
    }
  
    public getPosition(): { x: number; y: number } {
      return { x: this.x, y: this.y };
    }
  
    public setPosition(x: number, y: number): void {
      this.x = x;
      this.y = y;
    }

    public getSprite(): Sprite {
        return this.sprite;
    }

    public setSprite(sprite: Sprite): void {
        this.sprite = sprite;
    }

    public getBounds(): Rectangle {
        // Calculate and return the bounding rectangle of the entity
        const bounds = this.sprite.getBounds();
        bounds.x = this.x;
        bounds.y = this.y;
        return bounds;
      }
      public destroy(): void {
        // Make the sprite invisible
        this.sprite.visible = false;

        // Remove the sprite from its parent container or stage
        if (this.sprite.parent) {
          this.sprite.parent.removeChild(this.sprite);
        }
      }

  }
  