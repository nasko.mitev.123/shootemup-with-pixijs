import { Sprite } from "pixi.js";

import { Entity } from './Entity';

export class Obstacle extends Entity {
  //TODO Add any additional properties specific to the Obstacle
  protected _speed: number = 20; 
  constructor(x: number, y: number, sprite: Sprite) {
    super(x, y, sprite);
  }

  public get speed(): number {
    return this._speed;
  }

  public set speed(value: number) {
    this._speed = value;
  }

  public update(delta: number): void {
    this.moveLeft(delta)
  }

  public moveLeft(delta: number): void {
    this.x -= this.speed/100 * delta;
    // Additional logic for handling leftward movement, such as boundary checks
  }
}
