import { Sprite } from "pixi.js"
import { Entity } from './Entity';
import { GameConfig } from '../config/GameConfig';

export class Bullet extends Entity {
  private speed: number;
  
  constructor(x: number, y: number, sprite: Sprite, speed: number) {
    super(x, y, sprite);
    this.speed = speed;
  }

  public update(): void {
    // Update bullet logic based on delta and speed
    this.x += this.speed ;
  }

  public isOutOfBounds(): boolean {
    // Check if the bullet is out of bounds based on game boundaries
    const gameWidth = GameConfig.screenWidth; // Adjust to actual game width
    const gameHeight = GameConfig.screenHeight; // Adjust to actual game height

    return this.x < 0 || this.x > gameWidth || this.y < 0 || this.y > gameHeight;
  }

}
