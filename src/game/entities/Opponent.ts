import { Sprite } from "pixi.js";
import { Entity } from './Entity';
import { OpponentBullet } from './OpponentBullet';

export class Opponent extends Entity {
  //TODO Add any additional properties specific to the Opponent
  protected _speed: number = 5; // Adjust the speed as needed
  public bullets: OpponentBullet[];
  public shootSpeed: number = 7;
  private lastBulletSpawnTime: number;
  private bulletSpawnCooldown: number = 5000; // Set the desired cooldown between bullet spawns (in milliseconds)
  constructor(x: number, y: number, sprite: Sprite) {
    super(x, y, sprite);
    this.bullets = [];
    this.lastBulletSpawnTime = 0;
  }

  public get speed(): number {
    return this._speed;
  }

  public set speed(value: number) {
    this._speed = value;
  }

  public update(delta: number): void {
    this.moveLeft(delta);
    this.fireProjectile();
    this.updateBullets();
  }

  public fireProjectile(): void {
    const currentTime = Date.now();
    if (currentTime - this.lastBulletSpawnTime >= this.bulletSpawnCooldown) {
    const bulletSprite = Sprite.from('../../assets/sprites/bullet01.png');
    const bullet = new OpponentBullet(this.x, this.y, bulletSprite, this.shootSpeed);
    this.bullets.push(bullet);
  

    this.lastBulletSpawnTime = currentTime; // Update the last bullet spawn time
    }
  }
  

  public moveLeft(delta: number): void {
    this.x -= this.speed/100 * delta;
    // Additional logic for handling leftward movement, such as boundary checks
  }

  private updateBullets(): void {
    for (let i = this.bullets.length - 1; i >= 0; i--) {
      const bullet = this.bullets[i];
      bullet.update();
      // Additional logic for bullet movement, collision detection, etc.
      // Remove bullets that are out of bounds or have collided with something
      if (bullet.isOutOfBounds()) {
        bullet.destroy();
        this.bullets.splice(i, 1);
      }
    }
  }
}
