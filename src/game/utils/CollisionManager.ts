import { Entity } from "../entities/Entity";

export class CollisionManager {
  constructor() {
    // Additional setup if needed
  }

  public checkCollision(entity1: Entity, entity2: Entity): boolean {
     // Retrieve the bounding rectangles of the entities
     const rect1 = entity1.getBounds();
     const rect2 = entity2.getBounds();
 
     // Perform a basic bounding box collision detection
     if (
       rect1.x < rect2.x + rect2.width &&
       rect1.x + rect1.width > rect2.x &&
       rect1.y < rect2.y + rect2.height &&
       rect1.y + rect1.height > rect2.y
     ) {
       // Collision detected
       return true;
     }
 
     // No collision detected
     return false;
  }

}
