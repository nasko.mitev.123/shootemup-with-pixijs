import { Sprite, Container } from "pixi.js";
import { ISpawner } from '../interfaces/ISpawner';
import { GameConfig } from '../config/GameConfig';
import { Opponent } from '../entities/Opponent';
import { Entity } from '../entities/Entity';
import { EntityType } from './Enums';
import { Obstacle } from '../entities/Obstacle';
import { GroundOpponent } from '../entities/GroundOpponent';

export class Spawner implements ISpawner {
  private spawnTimer: number;
  private spawnInterval: number;
   // Adjust this value to control the spawn speed

  constructor() {
    this.spawnTimer = 0;
    this.spawnInterval = 2000;
  }

  public spawn(delta: number, entities: Entity[], container: Container, entityType: EntityType, 
    spawnSpeed:number): void {

    // Increment the spawn timer based on elapsed time
    this.spawnTimer += delta;
    this.adjustSpawnInterval(spawnSpeed);
    const screenHeight = GameConfig.screenHeight;
    const screenWidth = GameConfig.screenWidth;
    let randomY;

    // Check if it's time to spawn a new enemy
    if (this.spawnTimer >= this.spawnInterval) {
      switch(entityType) {
        case EntityType.Opponent: 
          const opponentSprite = Sprite.from('../../assets/sprites/opponent01.png');
          randomY = Math.random() * (screenHeight - opponentSprite.height-80);
          const opponent = new Opponent(screenWidth, randomY, opponentSprite); // Spawn at random position
          entities.push(opponent);
          container.addChild(opponent.getSprite());
          break;

          case EntityType.Obstacle: 
          const obstacleSprite = Sprite.from('../../assets/sprites/obstacle.png');
          
          randomY = Math.random() * (screenHeight - obstacleSprite.height-80);
          const obstacle = new Obstacle(screenWidth, randomY, obstacleSprite); // Spawn at random position
          entities.push(obstacle);
          container.addChild(obstacle.getSprite());
          break;

          case EntityType.GroundOpponent: 
          const groundOpponentSprite = Sprite.from('../../assets/sprites/opponent02.png');
          
          const positionY = screenHeight -80;
          const groundOpponent = new GroundOpponent(screenWidth, positionY, groundOpponentSprite); // Spawn at random position
          entities.push(groundOpponent);
          container.addChild(groundOpponent.getSprite());
          break;
            };
      
      
  
      // Reset the spawn timer
      this.spawnTimer = 0;
    }
  
    // Adjust the spawn interval at a fixed interval
    const spawnIntervalAdjustmentInterval = 5000; // Adjust this value as desired
    if (this.spawnTimer >= spawnIntervalAdjustmentInterval) {
      this.adjustSpawnInterval(spawnSpeed);
    }
  }

private adjustSpawnInterval(desiredEnemiesPerSecond: number): void {
  

  // Calculate the spawn interval based on desired enemies per second
  const millisecondsPerSecond = 1000;
  this.spawnInterval = Math.floor(millisecondsPerSecond / desiredEnemiesPerSecond);

  // Ensure a minimum spawn interval to avoid excessive spawning
  const minimumSpawnInterval = 5000; // Adjust this value as needed
  this.spawnInterval = Math.max(this.spawnInterval, minimumSpawnInterval);
}
}
