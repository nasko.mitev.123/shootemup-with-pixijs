import { Loader, Texture } from "pixi.js";

export class AssetLoader {
    private loader: Loader;
  
    constructor() {
      this.loader = new Loader();
    }
  
    public loadAssets(assetPaths: string[]): Promise<void> {
      return new Promise((resolve, reject) => {
        this.loader.add(assetPaths).load(() => {
          resolve();
        });
  
        this.loader.onError.add((error: any) => {
          reject(error);
        });
      });
    }
  
    public getTexture(textureName: string): Texture | undefined {
        const resource = this.loader.resources[textureName];
    
        if (resource && resource.texture) {
          return resource.texture;
        }
    
        // Return undefined if the texture is not found
        return undefined;
      }
  }
  