export class InputManager {
    private keys: { [key: string]: boolean };
    private isPointerDown: boolean;
  
    constructor() {
      this.keys = {};
      this.isPointerDown = false;
    }

    public initInputHandling(): void {
       // Set up event listeners for keyboard and pointer input
      window.addEventListener('keydown', this.handleKeyDown.bind(this));
      window.addEventListener('keyup', this.handleKeyUp.bind(this));
      }
  
    private handleKeyDown(event: KeyboardEvent): void {
      this.keys[event.code] = true;
      // Additional logic for handling keydown events
    }
  
    private handleKeyUp(event: KeyboardEvent): void {
      this.keys[event.code] = false;
      // Additional logic for handling keyup events
    }
  
  
   public isKeyPressed(keyCode: string): boolean {
  return this.keys[keyCode];
}
  
    public isPointerPressed(): boolean {
      return this.isPointerDown;
    }

  
  }
  