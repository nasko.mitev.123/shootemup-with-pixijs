export enum EntityType {
    Opponent = 'OPPONENT',
    Obstacle = 'OBSTACLE',
    GroundOpponent = 'GROUNDOPPONENT',
  }