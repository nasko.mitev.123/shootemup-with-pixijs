export class ScoreManager {
    private score: number;
  
    constructor() {
      this.score = 0;
    }
  
    public getScore(): number {
      return this.score;
    }
  
    public increaseScore(points: number): void {
      this.score += points;
    }
  
    public resetScore(): void {
      this.score = 0;
    }
  
  }
  