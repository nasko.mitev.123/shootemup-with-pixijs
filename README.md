Description:
This project uses PIXI.js and TypeScript to create a side-scrolling shoot em up game, the game features
a movable player, 2 types of enemies, a scrolling background, obstacles,  score, lives, abilities for the player
to shoot and drop bombs.

The game features a Start Screen with a play button. Press the play button with left mouse click to start
the game.

How to play the game:

Move the player Up: Up Arrow Key
Move the player Down: Down Arrow Key
Move the player Left: Left Arrow Key
Move the player Right: Right Arrow Key
Shoot: Spacebar Key
Drop Bombs: Z Key

Avoid obstacles and shoot the enemies the rank up your score! If you run out the lives the game will end
and you will be given the option to restart by pressing the restart button with left mouse click.

Disclaimer!
The game is not finished and is in a playable state of what is considered the "base game". It is still a work in progress and to check the progress of the project please see the ClickUp project as it holds information regarding what is complete, what is still being worked on and and what is planned to be added.

Link for ClickUp project:
https://sharing.clickup.com/9004113219/l/h/8cazqa3-301/b5d039978f49e2f

The game also has a live Demo which you can see here:
http://shootemup-with-pixi.great-site.net

How to start the project:

> Run `npm i` before first start

> Run (dev): `npm start` then browse http://localhost:8080/ 

For problems with sprites please install "Allow CORS: Access-Control-Allow-Origin" plugin for Chrome
if you are using it as the browser to play the game. Thank you!

Link for plugin: https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf/related?hl=en&gclid=CjwKCAjw44mlBhAQEiwAqP3eVuUa71EknkfmpmiCgfobrNZb4xgIFYqu0VKPHmHqkOYWP5Z4KnWEZhoCflkQAvD_BwE